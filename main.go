package main

import "net/http"

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Second: master"))
	})
	http.ListenAndServe(":7002", nil)
}
